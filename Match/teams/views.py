from django.shortcuts import render
from django.http import HttpResponse
from .models import *

# Create your views here.

def home(request):

    team = Team.objects.all()
    return render(request, 'teams/index.html', {'team' : team })


def players(request):
    players = Player.objects.all()
    return render(request, 'players/players.html', {'players':  players })


def match(request):
    matches = Matches.objects.all()
    return render(request, 'matches/match.html', {'matches' : matches })

def points(request):
    points = Points.objects.all()
    return render(request, 'points/point.html', {'points' : points })


